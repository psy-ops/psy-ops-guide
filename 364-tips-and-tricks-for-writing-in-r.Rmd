# Tips and Tricks for Writing in R {#tips-and-tricks-for-writing-in-r}

## Finding the right words

The package `syn` lets you find synonyms, for example:

```r
syn::syn('open')
```

You can limit the number of words by specifying a second argument, in which case a random selection is returned. The package also lets you search for antonyms by using `syn::ant()`. For more information, see https://syn.njtierney.com/.
