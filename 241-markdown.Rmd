# Markdown {#markdown}

You can consider Markdown like the sweet spot between, on the one hand, plain text, and on the other hand, advanced markup languages and file formats like HTML, LaTeX, ODT, Docx, and RTF.

The huge advantage of plain text is that it is plain. And as such, it is, as much as possible, interoperable, and easy to read for humans. However, the drawback is that it does not provide any means to specify text markup, so you cannot have titles, or tables, or emphasized text, or even paragraphs.

On the other hand, dedicated markup languages such as the Hyper Text Markup Language (HTML), the sophisticated typesetting language LaTeX, and word processor file formats such as ODT, Docx, and RTF are expressive but as a consequence, cannot be read easily by humans without having the corresponding software installed.

Markdown addresses the gap between these two extremes. It consists of a simple convention of how to specify markup information in plain text files in a way that retains the human-readability of those text files. It does this by being heavily based on the informal conventions humans started adopting when communicating in plain text channels, e.g. plain text emails or SMS messages.

Markdown was developed in 2004 by [John Gruber](https://daringfireball.net/projects/markdown/). Since then it has grown into a popular and versatile format for writing content without being distracted by styling.

Many online tutorials and guides exist, such as the 10-minute https://markdowntutorial.com, the Common Mark tutorial at https://commonmark.org/help/tutorial/, the slightly more technical https://markdownguide.org, and the original Markdown syntax specification at https://daringfireball.net/projects/markdown/syntax. The basics will be provided in this Chapter.

## Basic layout

To emphasize something, wrap it in a single asterisk to make it italic, and two asterisks to make it bold. For example, if "`this *Markdown* text is **rendered**`", it will display as "this *Markdown* text is **rendered**". Of course, these two can **also* be combined* (which is specified as "`**also* be combined*`".

A paragraph break consists of two consecutive line breaks (i.e. two 'enters'), or in other words, an empty line. If you insert multiple empty lines, they are simply ignored. Single line breaks are also ignored and processed as if they are a space. Similarly, consecutive spaces are rendered as one space. If you want to go to the next line, but you don't want to start a new paragraph, end the previous line with  
two spaces.

Headings can be inserted by prepending one or more hashes (pound signs, i.e. the "`#`"), followed by a space, before the heading. The number of hashes signifies the depth of the heading; i.e. the 'biggest' headings have one hash, level 2 headings have 2 hashes, etc.

## Figures and Tables


