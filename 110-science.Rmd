# Science {#science}

Science is a systematic approach to discovering how the world works. The systematic nature of this approach makes it possible to take the humans out of the equation as much as possible. Removing humans from the equation is important because humans are good at perceiving patterns, a skill that sometimes causes patterns to be perceived where none exist.

In other words, when you've become convinced you identified a pattern in the world based on your observations, you will often be correct, and you will often be incorrect. There is only one way to distinguish correctly identified patterns from your psychological machinery being overenthusiastic in its pattern detection, and that is interacting with the world. In other words, testing whether you're right by collecting data.

By systematically designing procedures for manipulating bits of the world and collecting data, then actually collecting the data, analysing the data, and correcly interpreting the results, it becomes possible to minimize biases and make them visible where minimization is not possible. If the pattern you identified is correct, and not the result of overenthusiastic but misguided pattern detection, it is possible to design procedures such that you yourself can be taken out of the equation.

If you cannot devise procedures that can be followed by others, and that allow them to draw the same conclusions about those patterns, it's pretty likely you're deluding yourself. This places the experiment firmly at the core of the scientific method. If you understand something about the world, you can reliably manipulate it and observe the effects.

By applying this logic, the scientific method turned out to be pretty much the only reliable source of information about the world around us. The insights obtained through this method have many uses, the most obvious of which manifest through applied science. Applied science uses the knowledge about patterns in reality to manipulate the world around us. Examples of applied science are technologies like sewage systems, bicycles, vaccines, cars, batteries, computers, airplanes, prosthetics, and the internet.

The complement of applied science is basic science (or somewhat pretentiously, pure science). Basic science aims to generate knowledge, regardless of any direct potential for application; applied science aims to leverage the knowledge gained through basic science to address real-world problems and challenges.

Successful application requires that the identified patterns are correct. You can't build a car if your combustion engine explodes half the time (well, not a safe car). Developing knowledge into technology first requires reliable knowledge. Patterns that have been identified with that degree of reliability can usually be expressed mathematically.

However, initially, expressing suspected or identified patterns happens qualitatively, simply using natural language such as English, Hungarian, Mandarin, or Dutch.[^Some would argue that some scientific disciplines, due to their nature, can never discover patterns that are usefully expressed mathematically.] This is particularly understandable when the subject of study is something people already discuss all the time: people. This is the case for the science of psychology.
