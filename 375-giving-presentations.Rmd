# Giving presentations {#giving-presentations}

This Chapter contains a number of tips for giving a presentation.


- In the room, make sure you can see the slides without having to turn to the screen your audience sees. In other words, there should be a screen (e.g. the PC sending the slides to the projector or screen viewed by your audience) that you can see when you stand with your face towards your audience. It's important to make sure you don't end up presenting to the large screen behind you, instead of to your audience.

- If you forget something, you can safely ignore that. Your audience has no idea; they will believe that whatever ou present to them is what you intend to present to them. Therefore, it is less distracting and confusing for your audience if you don't bother them by explaining such omissions.

- During preparation, it can be useful to contact the organisers and verify the following:

  - What is the aspect ratio of the screen that is used? The default nowadays is 16:9, but older set-ups may still use 4:3.
  - Can your own PC be used, or will you need to use a PC provided by the organisers?
  - In the latter case:
    - Don't forget to embed the fonts in your presentation file!
    - What software and version are used on the PC used for the presentation? For example, the "Morph" slide transition was only introduced in PowerPoint 2019. It often works in PowerPoint 2016, but will fail in earlier versions.
    - Make sure the PC you'll used doesn't have a very restrictive security policy in place - this can sometimes prohibit the use of embedded fonts.
  - Is internet available?
  - Is the set-up such that speakers can view the laptop screen simultaneously while viewing the audience?
  - Is a timer visible for the speaker, so that speakers can flexibly adjust the speed during the presentation?
  - Is there a microphone? Even if you can speak quite loudly, for hearing impaired people, understanding somebody who speaks loudly is much harder than when proper amplification is used.


